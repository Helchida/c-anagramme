#include "anagrammes.h"
#include <stdio.h>
#include <sys/time.h>

#define BUFSIZE 512

int main(int argc, char *argv[]) {
  char buf[BUFSIZE];
  //Déclaration + création du tableau de mot (dictionnaire sous forme de tableau)
  struct word_array array_dictionnaire;
  word_array_create(&array_dictionnaire);

  //Met le fichier dictionnaire.txt dans mon tableau de mot array_dictionnaire
  word_array_read_file(&array_dictionnaire,"dictionnaire.txt");
  
  //Déclaration + création d'une table de hachage (dictionnaire sous forme de tableau de liste)
  struct word_dict bucket_dictionnaire;
  word_dict_create(&bucket_dictionnaire);

  //Convertit le tableau dictionnaire en table de hachage avec le contenu
  word_dict_fill_with_array(&bucket_dictionnaire,&array_dictionnaire);
  
  //Déclaration des structures de temps
  struct timeval debut;
  struct timeval fin;
  
  //Boucle infini qui permet la saisie de mot
  for (;;) {
    printf("Pour quitter le programme, appuyez sur la touche Entrée.\n ");
    printf("Saisir le mot dont vous souhaitez connaître les anagrammes : (uniquement des lettres de l'alphabet sans majuscules, ni accents, ni ponctuations ");
    //On récupere le mot écrit au clavier
    fgets(buf, BUFSIZE, stdin);
    //Si l'utilisateur saisit Entrée on sort de la boucle
    if(buf[0]=='\n'){
      break;
    }
    clean_newline(buf, BUFSIZE);

    //Déclaration + création des tableaux de résultats pour le tableau et pour la table de hachage
    struct word_array array_result;
    word_array_create(&array_result);
    struct word_array bucket_result;
    word_array_create(&bucket_result);

    //Calcul du temps d'exécution de la recherche des anagrammes dans le tableau pour le mot saisit + recherche + affichage du tableau de résultat d'anagramme(s) trouvé(s)
    printf("Résultat en utilisant la méthode des tableaux :\n");
    gettimeofday(&debut,NULL);
    word_array_search_anagrams_wildcard(&array_dictionnaire,buf,&array_result);
    gettimeofday(&fin,NULL);
    time_t temps_microsecondes= fin.tv_usec-debut.tv_usec;
    word_array_print(&array_result);
    printf("Temps mis lors de l'execution de word_array_search_anagrams_wildcard: %zu µs \n",temps_microsecondes);

    //Calcul du temps d'exécution de la recherche des anagrammes dans la table de hachage pour le mot saisit + recherche + affichage du tableau de résultat d'anagramme(s) trouvé(s)
    printf("Résultat en utilisant la méthode des tables de hachages :\n");
    gettimeofday(&debut,NULL);
    word_dict_search_anagrams_wildcard(&bucket_dictionnaire,buf,&bucket_result);
    gettimeofday(&fin,NULL);
    temps_microsecondes= fin.tv_usec-debut.tv_usec;
    word_array_print(&bucket_result);
    printf("Temps mis lors de l'execution de word_dict_search_anagrams_wildcard: %zu µs \n",temps_microsecondes);

    //Destruction des tableaux de résultat
    word_array_destroy(&array_result);
    word_array_destroy(&bucket_result);
  }
  //Destruction du tableau ainsi que de la table de hachage contenants le dictionnaire
  word_array_destroy(&array_dictionnaire);
  word_dict_destroy(&bucket_dictionnaire);
  return 0;
}