#include "anagrammes.h"

#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#define BUFSIZE 512
#define FNV_PRIME 1099511628211
#define FNV_OFFSET_BASIS 14695981039346656037ul

/***********************************************************************************
// Partie 1
************************************************************************************/

//Fonction qui compare deux tableau d'entier passés en paramètre et qui renvoie true si ils sont égaux, false sinon.
static bool arraycmpInt(int *str1,int *str2,int size){
  for(int i=0;i<size;i++){
		if(str1[i]!=str2[i]){
			return false;
		}
	}
  return true;
}

//Fonction qui calcule la longueur d'une chaine de caractères sans compter le caractere final ('\n').
static size_t str_length(const char *str) {	
	size_t len=0;
    /*Si la liste est NULL elle ne contient rien donc on return bien 0.*/
	if (str==NULL){
		return 0;
	}else{
        /*Parcours de str jusqu'a trouvé le caractere final '\0' a chaque caractere on augmente de 1 la taille de str.*/
		while (str[len]!='\0'){
			len++;
		}
	  return len;
	}
}
//Fonction qui renvoie TRUE si les deux chaines de caractere passé en parametre sont bien anagrammes FALSE sinon.
bool string_are_anagrams(const char *str1, const char *str2) {
  /*str3 et str4 sont des tableaux de int. 
  On considere que l'indice 0 signifie le nombre de lettre a etc... jusqu'a l'indice 25 pour la lettre z.*/
  int *str3=calloc(26,sizeof(int));
  int *str4=calloc(26,sizeof(int));
  size_t i=0;
  size_t j=0;
  /*Parcours du tableau str1 qui permet de compter le nombre de chaque caractere*/
  while(str1[i]!='\0'){
    ++str3[str1[i]-'a'];
    ++i;
  }
  /*Parcours du tableau str2 qui permet de compter le nombre de chaque caractere*/
  while(str2[j]!='\0'){
    ++str4[str2[j]-'a'];
    ++j;
  }
  /*Comparaison des caracteres contenus dans str1 et str2*/
  int nb=arraycmpInt(str3,str4,26);
  /*liberation de str3 et str4 qui servent uniquement dans cette fonction*/
  free(str3);
  free(str4);
  return nb;
}

//Fonction qui permet de dupliquer une chaine de caractere passe en parametre.
char *string_duplicate(const char *str) {
  /*Cas ou la chaine de caractere est NULL, il sufit de retourner NULL.*/
  if (str==NULL){
    return NULL;
  }
  /*Allocation de l'espace memoire necessaire pour dupliquer str.*/
  size_t i=0;
  size_t longueur=str_length(str);
  char *str2=malloc(longueur*sizeof(char*));  /* Impossible de liberer str2 ici car sinon on return un chaine de caractere vide.*/
  /*Parcours de str pour copier un à un les caracteres dans str2.*/
  for(i=0;i<longueur;i++){
    str2[i]=str[i];
  }
  str2[i]='\0';
  return str2;
	
}

//Fonction qui permet de trier une chaine de caracatere dans l'ordre alphabetique.
void string_sort_letters(char *str) {
  /*Utilisation de la technique du tri par Insertion.*/
  size_t n=str_length(str);
  for(size_t i = 1 ; i<n ; i++){
    char x=str[i];
    size_t j=i;
    while(j>0 && str[j-1]>x){
      str[j]=str[j-1];
      j--;
    }
    str[j]=x;
  }
}

//Fonction qui permet de supprimer le caractere de changement de ligne pour le remplacer par '\0'.
void clean_newline(char *buf, size_t size) {
 size_t i=0;
 /*Obtention de l'indice ou se trouve le caractere '\n'.*/
 while(buf[i]!='\n'){
   ++i;
 }
/*Remplacement de '\n' par '\0'.*/
 buf[i]='\0';
}

/***********************************************************************************
// Partie 2
************************************************************************************/

//Fonction qui permet de multiplier par 2 la taille aloue du tableau.
static void growth_capacity_word_array(struct word_array *self){
  /*Pour eviter de faire cette fonction a chaque fois on multiplie la taille de la capaciter par deux.*/
	self->capacity=(self->capacity)*2;
	char **newdata=malloc(self->capacity*sizeof(char*));	
	memcpy(newdata,self->data,self->size*sizeof(char*));
	free(self->data);
	self->data=newdata;
}

//Fonction qui permet de creer une structure word_array_create en l'initialisant.
void word_array_create(struct word_array *self) {
  /*On initialise la taille a 0 car le tableau ne contient aucun element.*/
  self->size=0;
  /*On initialise la capacite a 100 de maniere arbitraire car il faut lui donner une valeur diffeente de 0.*/
	self->capacity=100;
  /*Allocation de la place necessaire pour pouvoir remplir le tableau.*/
	self->data=malloc(self->capacity *sizeof(char*));
}

//Fonction qui permet de detruire un tableau de mot.
void word_array_destroy(struct word_array *self) {
  /*Parcours de data en liberant a chaque fois la chaine de caracatere contenue dans data[i].*/
  for (size_t i=0;i<self->size;++i) {
	  free(self->data[i]);
  }
  /*Supression de data.*/
  free(self->data);
}

//Fonction qui permet d'ajouter le mot word dans le tableau self.
void word_array_add(struct word_array *self, const char *word) {
  /*Si il n'y a plus assez de place dans self pour ajouter le mot on appel une fonction qui augmente la taille.*/
  if(self->size==self->capacity){
		growth_capacity_word_array(self);
	}
  /*On duplique le mot car ici notre word est en const.*/
  char *new_Word=string_duplicate(word);
  self->data[self->size]=new_Word;
  self->size++;

}

//Fonction qui recherche les anagrammes du mot word dans le tableau self, tous ceux qui sont trouves sont places dans result.
void word_array_search_anagrams(const struct word_array *self, const char *word, struct word_array *result) {
  /*Parcours de tous le tableau self.*/
  for(size_t i=0;i<self->size;++i){
    /*Si le mot contenue dans self est annagramme de word alors on l'ajoute dans le tableau result.*/
    if(string_are_anagrams(word,self->data[i])){
      word_array_add(result,self->data[i]);
    }
  }
}

//Fonction qui permet d'echanger deux variables (partie du tri rapide).
static void word_array_swap(struct word_array *self,size_t i, size_t j){
  char *temp =self->data[i];
  self->data[i]=self->data[j];
  self->data[j]=temp;
}

//Fonction qui permet de partitionner le tableau self (partie du tri rapide).
static ssize_t word_array_partition(struct word_array *self, ssize_t i, ssize_t j) {
  ssize_t pivot_index = i;
  char *pivot = self->data[pivot_index];
  word_array_swap(self, pivot_index, j);
  ssize_t l = i;
  for (ssize_t k = i; k < j; ++k) {
    if (strcmp(self->data[k],pivot)<0) {
      word_array_swap(self, k, l);
      l++;
    }
  }
  word_array_swap(self, l, j);
  return l;
}

//Fonction qui permet d'apeller une fonction pour partitionner le tableau(partie du tri rapide).
static void word_array_quick_sort_partial(struct word_array *self,ssize_t i, ssize_t j) {
  if (i < j) {
    ssize_t p = word_array_partition(self, i, j);
    word_array_quick_sort_partial(self, i, p - 1);
    word_array_quick_sort_partial(self, p + 1, j);
  }
}

//Fonction qui permet d'executer le tri rapide.
void word_array_sort(struct word_array *self) {
  word_array_quick_sort_partial(self, 0, self->size - 1);
}


//Fonction qui permet d'aficher un tableau de mot.
void word_array_print(const struct word_array *self) {
  size_t i=0;
  /*Parcours du tableau self.*/
    for(i=0;i<self->size;i++){
      /*Affichage du mot contenu la case i de self.*/
      printf("%s\n",self->data[i]);
    }
    /*Afichage du nombre d'annagrammes trouves.*/
    printf("\nNombre d'anagramme(s) trouvé(s): %lu\n",i);
}

//Fonction qui permet de lire un fichier
void word_array_read_file(struct word_array *self, const char *filename) {
  char word[WORD_LETTERS_MAX];

  FILE *fp = fopen(filename, "r");

  if (fp == NULL) {
    fprintf(stderr, "Error when opening file '%s'\n", filename);
    return;
  }

  while (!feof(fp)) {
    if(fgets(word, WORD_LETTERS_MAX, fp)){
      clean_newline(word, WORD_LETTERS_MAX);
      word_array_add(self, word);
    }
  }

  fclose(fp);
}

/***********************************************************************************
// Part 3
************************************************************************************/

//Fonction qui permet de detruire "proprement" un word_dict_bucket.
void word_dict_bucket_destroy(struct word_dict_bucket *bucket) {
  /*Si le bucket est NULL inutile de continuer car il est deja vide.*/
  if(bucket==NULL){
    return;
  }
  struct word_dict_bucket *suppri=bucket;
  /*Parcours de liste suppri.*/
  while(suppri!=NULL){
    struct word_dict_bucket *supprimer= suppri;
    suppri=suppri->next;
    /*Supression a chaque etape de supprimer.*/
    free(supprimer);
  }
}

//Fonction qui permet d'ajouter un mot word au debut de la liste chaine bucket.
struct word_dict_bucket *word_dict_bucket_add(struct word_dict_bucket *bucket, const char *word) {
  /*Si la liste n'est pas vide on ajoute alors l'ancienne liste dans le next de la nouvelle qui cotient word.*/
	if (bucket!=NULL){
    struct word_dict_bucket *nouveau=malloc(sizeof(struct word_dict_bucket));
	  nouveau->next=bucket;
    nouveau->word=word;
		return nouveau;
	}
	/*Sinon, donc si la liste est vide il suffit d'ajouter word a la liste et de mettre next a NULL.*/
  bucket=malloc(sizeof(struct word_dict_bucket));
  bucket->word=word;
  bucket->next=NULL;
  return bucket;
}

//Fonction qui permet de creer un dictionnaire vide.
void word_dict_create(struct word_dict *self) {
  self->size=0;
  self->count=0;
  self->buckets=malloc(sizeof(struct word_dict_bucket));
}

//Fonction qui permet de detruire "proprement" un dictionnaire
void word_dict_destroy(struct word_dict *self) {
  size_t i=0;
  /*Parcours des listes du dictionnaire.*/
  for(i=0;i<self->size;++i){
    /*Suppression une à une des listes du dictionnaire.*/
    word_dict_bucket_destroy(self->buckets[i]);
  }
  /*Liberation du tableau de bucket.*/
  free(self->buckets);
  /*Remise a 0 de size et count car la structure ne contient plus rien.*/
  self->size=0;
  self->count=0;
}

//Fonction permet de "hacher" un mot key selon la hash FNV-la 64 bits.
size_t fnv_hash(const char *key) {
  /*Duplication du mot key, car ici c'est une CONST.*/
  char *newkey=string_duplicate(key);
  /*Tri du mot key dans l'ordre alphabetique.*/
  string_sort_letters(newkey);
  size_t hash=FNV_OFFSET_BASIS;
  /*Parcours des caracteres du mot key en appliquant le hachage a chaque caractere.*/
  for(size_t i=0;i<str_length(newkey);++i){
    hash=hash ^ newkey[i]; 
    hash=hash * FNV_PRIME;
  }
  free(newkey);
  return hash;
}

//Fonction qui effectue un rehash
void word_dict_rehash(struct word_dict *self) {
  //Creer nouveau tableau avec taille 2* plus grande si plus de place.
  struct word_dict new_word_dict;
  new_word_dict.size=(self->size + 1)*2;
  new_word_dict.count=self->count;
  new_word_dict.buckets=calloc(new_word_dict.size,sizeof(struct word_dict_bucket));
	for(size_t i=0;i<self->size;++i){
      struct word_dict_bucket *parcour=self->buckets[i];
      /*Recalcule des nouveaux indices de tout les anciens mot contenue dans self.*/
      while(parcour!=NULL){
        word_dict_add(&new_word_dict,parcour->word);
        parcour=parcour->next;
      } 
    }
  word_dict_destroy(self);
  self->buckets=new_word_dict.buckets;
  self->size=new_word_dict.size;
}

//Fonction qui permet d'ajouter une entrée dans le dictionnaire self.
void word_dict_add(struct word_dict *self, const char *word) {
  /*Si l'on arrive a la moitie de la taille du dictionnaire on effetue un rehash.*/
  if(self->count>=self->size/2){
    word_dict_rehash(self);
  }
  /*fnv_hash(word)%self->size permet de calucler l'indice ou le mot doit etre mis dans le tableau self.*/
  self->buckets[fnv_hash(word)%self->size]=word_dict_bucket_add(self->buckets[fnv_hash(word)%self->size],word);
  self->count++;
}

//Fonction qui remplit un dictionnaire avec les mots contenus dans un tableau de mot.
void word_dict_fill_with_array(struct word_dict *self, const struct word_array *array) {
  //On parcourt notre tableau de mot et on ajoute le mot dans le dictionnaire.
  for(size_t i=0;i<array->size;++i){
    word_dict_add(self,array->data[i]);
  }
}

//Fonction qui cherche des anagrammes dans une table de hachage
void word_dict_search_anagrams(const struct word_dict *self, const char *word, struct word_array *result) {
  struct word_dict_bucket *parcour=self->buckets[fnv_hash(word)%self->size];
  //On parcourt notre liste de chaques index jusqu'à la fin.
  while(parcour!=NULL){
    //Si word et parcour->word sont des anagrammes on ajoute parcour->word à result.
    if(string_are_anagrams(word,parcour->word)){
      word_array_add(result,parcour->word);
    }
    parcour=parcour->next;
  } 
}

/***********************************************************************************
// Part 4
************************************************************************************/

//Fonction qui initialise une structure wildcard avec count=0 et chaques cases prend pour valeur BUFSIZE+1.  
void wildcard_create(struct wildcard *self) {
  self->count=0;
  for(size_t i=0;i<WILDCARDS_MAX;++i){
    self->index[i]=BUFSIZE+1;
  }
}
//Fonction qui trouve combien il y a de caractere '*' dans un mot saisit (maximum 4).
void wildcard_search(struct wildcard *self, const char *word) {
  //On parcourt le mot en entier.
  for (size_t i=0;i<str_length(word);++i) {
    //Dans le cas où nous avons déjà 4 jokers on sort de la fonction.
    if (self->count==4){
      return;
    }
    //Dans le cas où on trouve un '*' dans le mot on ajoute 1 à self->count. 
    if (word[i]=='*') {
      self->index[self->count]=i;
      self->count++;
    }
  }
}
//Fonction qui calcule le nombre d'anagrammes qu'il y a pour un mot dans un tableau de mot.
static int how_many_anagrams(const struct word_array *possibilites, const char *mot){
  int cpt=0;
  //On répète possibilites->size fois.
  for(size_t i=0;i<possibilites->size;++i){
    //Si nos mots comparés sont des anagrammes on augmente le compteur d'anagrammes.
    if(string_are_anagrams(mot,possibilites->data[i])){
      cpt++;
    }
  }
  return cpt;
}

//Fonction qui rempli un tableau avec toutes les possibilités de mots selon le nombre de joker mais qui enlève les possibilités qui ont des doublons d'anagrammes.
static void array_possibilites (const char *word,struct word_array *possibilites) {
  struct wildcard joker;
  wildcard_create(&joker);
  wildcard_search(&joker,word);
  char *mot=string_duplicate(word);
  //Si on a au moins 1 joker
  if (joker.count>=1) {
    //On ajoute de 'a' à 'z' tout les mots avec la lettre en cours dans le premier joker.
    for (size_t i='a';i<='z';++i) {
      mot[joker.index[0]]=i;
      //Si on a au moins 2 jokers
      if (joker.count>=2) {
        //On ajoute de 'a' à 'z' tout les mots avec la lettre en cours dans le deuxième joker.
        for (size_t j='a';j<='z';j++) {
          mot[joker.index[1]]=j;
          //Si on a au moins 3 jokers
          if (joker.count>=3) {
            //On ajoute de 'a' à 'z' tout les mots avec la lettre en cours dans le troisième joker.
            for (size_t k='a';k<='z';++k) {
              mot[joker.index[2]]=k;
              //Si on a au moins 4 jokers
              if (joker.count==4) {
                //On ajoute de 'a' à 'z' tout les mots avec la lettre en cours dans le quatrième joker.
                for (size_t l='a';l<='z';++l) {
                  mot[joker.index[3]]=l;
                  //Si on a pas d'anagrammes on ajoute le mot dans un tableau de possibilité.
                  if(how_many_anagrams(possibilites,mot)==0){
                    word_array_add(possibilites,mot);
                  }
                  
                }
              } else {
                //Si on a pas d'anagrammes on ajoute le mot dans un tableau de possibilité.
                if(how_many_anagrams(possibilites,mot)==0){
                  word_array_add(possibilites,mot);
                }
              }
            }
          } else {
            //Si on a pas d'anagrammes on ajoute le mot dans un tableau de possibilité.
            if(how_many_anagrams(possibilites,mot)==0){
              word_array_add(possibilites,mot);
            }
          }
        }  
      }  else {
        //Si on a pas d'anagrammes on ajoute le mot dans un tableau de possibilité.
        if(how_many_anagrams(possibilites,mot)==0){
          word_array_add(possibilites,mot);
        }
      }
    }
    //Cas où il n'y a aucun joker dans le mot word on a joute le mot dans un tableau de possibilité.
  } else {
    word_array_add(possibilites,word);
  }
  free(mot);
}

//Fonction qui permet de rechercher des anagrammes selon le nombre de joker dans un tableau.
void word_array_search_anagrams_wildcard(const struct word_array *self, const char *word, struct word_array *result) {
  struct word_array possibilites;
  word_array_create(&possibilites);
  array_possibilites(word,&possibilites);
  //On parcourt notre tableau self en ajoutant tout les anagrammes de possibilites.data[i] dans le tableau result.
  for (size_t i=0;i<possibilites.size;++i) {
    word_array_search_anagrams(self,possibilites.data[i],result);
  }
  word_array_sort(result);
  word_array_destroy(&possibilites);

}

//Fonction qui permet de rechercher des anagrammes selon le nombre de joker dans une table de hachage.
void word_dict_search_anagrams_wildcard(const struct word_dict *self, const char *word, struct word_array *result) {
  struct word_array possibilites;
  word_array_create(&possibilites);
  array_possibilites(word,&possibilites);
  //On parcourt notre table de hachage self en ajoutant tout les anagrammes de possibilites.data[i] dans le tableau result.
  for(size_t i=0;i<possibilites.size;++i){
    word_dict_search_anagrams(self,possibilites.data[i],result);
  }
  word_array_sort(result);
  word_array_destroy(&possibilites);
}